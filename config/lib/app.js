const mongoose = require('mongoose');
const path = require('path');
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const userRoute = require(path.resolve('./modules/user/routes/users.route'));
const foodRoute = require(path.resolve('./modules/foods/routes/foods.route'));
const morgan  = require('morgan')
module.exports.start = function () {
  const app = express();
  let uri = "mongodb+srv://nguyenminhnhat:@123456@cluster0-i5icv.mongodb.net/test?retryWrites=true&w=majority";
  mongoose.connect(uri, { useNewUrlParser: true })
    .then(() => {
      console.log('Connect success database !');
    }).catch((err) => {
    console.log('Connect fail ');
    console.log('Error: ', err);
  });

  const port = process.env.PORT || 5000;
  app.use(cors());
  app.use(bodyParser.json());
  app.use(morgan('dev'));
  userRoute.init(app);
  foodRoute.init(app);
  app.get("/", async (request, response) => {
   return response.status(200).json({data: "123"});
  });
  app.listen(port, () => {
    console.log('Server run on port: ', port);
  })


};