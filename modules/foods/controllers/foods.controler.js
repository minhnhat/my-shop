'use strict';

/**
 * Module dependencies.
 */
var path = require('path');
var mongoose = require('mongoose');
var config = require(path.resolve('./config/config'));
var FoodsMD = require(path.resolve('./modules/foods/models/foods.model'));

exports.createFood = async function (req, res) {
  try{
    let data = req.body;
    let foodData = {
      shop: data.shop,
      name: data.name,
      type: data.type,
      image: data.image,
      image_extends: data.image_extends,
      created_at: new Date(),
      describe: data.describe,
      price:  data.price,
    };
    let food = new FoodsMD(foodData);
    let foodSaved = await food.save();
    return res.status(200).json({error: false, message:'Tạo thức ăn thành công',data: foodSaved });
  } catch (e) {
    console.error('e ', e);
    return res.status(400).json({error: true});
  }

};

exports.listFood = async function (req, res) {
  try{
    let shop = req.query.shop || '';
    if(!shop) return res.status(400).json({error: true, message: 'Shop không hợp lệ. Vui lòng kiểm tra lại'});
    let foods = await FoodsMD.find({shop: shop});
    if(!foods || (foods && !foods.length)) return res.status(400).json({error: false, message: 'Food is empty'});
    return res.status(200).json({error: false, data: foods});
  } catch (e) {
    console.error('listFood e ', e);
    return res.status(400).json({error: true, message: 'Có lỗi xảy ra trong quá trình xử lý. Vui lòng kiểm tra lại.'});
  }
};

exports.getFoodById = async function (req, res) {
  try{
    let shop = req.query.shop || '';
    let id = req.params.idFood || '';
    if(!shop || !id) return res.status(400).json({error: true, message: 'Dự liệu không hợp lệ. Vui lòng kiểm tra lại.'});
    let food = await FoodsMD.findOne({shop: shop, id: id})
    if(!food || (food && !food._id)) return res.status(200).json({error: false, message: 'Food không tồn tại trên hệ thống vui lòng kiểm tra lại.'});
    return res.status(200).json({error: false, data: food});
  } catch (e) {
    console.error('listFood e ', e);
    return res.status(400).json({error: true, message: 'Có lỗi xảy ra trong quá trình xử lý. Vui lòng kiểm tra lại.'});
  }
};