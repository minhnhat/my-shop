let path = require('path');
let mongoose = require('mongoose');
let config = require(path.resolve('./config/config'));
let foodController = require(path.resolve('./modules/foods/controllers/foods.controler'));

module.exports.init = function (app) {
  // Carriers collection routes
  app.route('/api/foods')
    .get(foodController.listFood)
    .post(foodController.createFood);

  app.route('/api/foods/:idFood')
    .get(foodController.getFoodById);

};


